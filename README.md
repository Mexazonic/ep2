# Exercício Programado 2

## Introdução

Aplicativo criado para critério avaliativo na linguagem JAVA.

Plataforma de Desenvolvimento: Android Studio.

O programa lista os Pokémons existentes referentes a API (PokéAPI).

## Pontos a serem destacados

* A Pokedéx relata os Pokemóns e seus respectivos sprites; 

* Não foram implementadas todos os requisitos exigidos por falta de tempo e habilidade do desenvolvedor;

* A interface é de caráter simplório;

* O cadastro do Treinador se encontra de forma incompleta;

* Foram utilizados os recursos de RecyclerView e floating bottom;

* A cahamada é carregada por gerações de pokemons;


### Considerações Finais

* A pokedéx exibe o nome e os sprites dos pokemons pela API solicitada, o aplicativo funcionou correntamente pelos emuladores de adroid presentes na IDE Android Studio. O apk gerado, encontra-se em: app/build/outputs/apk/debug. FOram utilizados bastantes materiais online e pdfś para a criação do projeto, o erro 'Parou de funcionar' é recorrente em funções não implementadas. Apesar dos constantes erros foi possível entregar algo para ser avaliado pois encontrei dificuldade em entender a linguagem java e suas relações entre classes e objetos com interface gráfica, o que infelizmente me fez perder muito tempo. 







