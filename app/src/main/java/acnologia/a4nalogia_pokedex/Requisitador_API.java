package acnologia.a4nalogia_pokedex;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Requisitador_API {
    @GET("pokemon")
    Call<Armazenamento_Info> chamada_api(@Query("limit") int limit, @Query("offset")int offset);


}
