package acnologia.a4nalogia_pokedex.Códigos_Centrais;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;

import acnologia.a4nalogia_pokedex.R;
import acnologia.a4nalogia_pokedex.Pokemon;
import acnologia.a4nalogia_pokedex.Armazenamento_Info;
import acnologia.a4nalogia_pokedex.Requisitador_API;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.Callback;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private Retrofit retrofit;
    private static final String TAG = "POKEDEX";

    private RecyclerView recyclerView;
    private Integrador_API dados_pokemons;

    private int offset;
    private boolean load_on;







    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        dados_pokemons= new Integrador_API(this);
        recyclerView.setAdapter(dados_pokemons);
        recyclerView.setHasFixedSize(true);

        final GridLayoutManager layoutManager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if(dy > 0){
                        int visibleItemCount = layoutManager.getChildCount();
                        int totalItemCount = layoutManager.getItemCount();
                        int pastVisibleItens = layoutManager.findFirstVisibleItemPosition();

                        if(load_on){
                            if((visibleItemCount+ pastVisibleItens) >= totalItemCount){
                                Log.i(TAG, "Aguardando Nova Geração");
                                load_on = false;
                                offset += 151;
                                pokemons_geracao(offset);
                            }
                        }
                }
            }
        });

        retrofit = new Retrofit.Builder()
                .baseUrl("http://pokeapi.co/api/v2/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

     load_on = true;
        offset = 0;
        pokemons_geracao(offset);

    }

    public void cadastrar (View v){
        startActivityForResult(new Intent(this, Mestres_Cadastro.class), 1);

    }

    public void cancelar (View v){

        startActivityForResult(new Intent(this, MainActivity.class), 1);

    }

    private void pokemons_geracao (int offset){
        Requisitador_API service = retrofit.create(Requisitador_API.class);
        Call<Armazenamento_Info> pokemonRespuestaCall = service.chamada_api(151, offset);

        pokemonRespuestaCall.enqueue(new Callback<Armazenamento_Info>() {
            @Override
            public void onResponse(Call<Armazenamento_Info> call, Response<Armazenamento_Info> response) {
              load_on = true;
                if (response.isSuccessful()){
                    Armazenamento_Info pokemonRespuesta = response.body();
                    ArrayList<Pokemon> set_datas = pokemonRespuesta.getResults();
                    dados_pokemons.adicionar_dados(set_datas);
                }   else {
                    Log.e(TAG, " loading     " + response.errorBody());
                }
            }



            @Override
            public void onFailure(Call<Armazenamento_Info> call, Throwable t) {
               load_on = true;
                Log.e(TAG, " fail "+ t.getMessage());
            }
        });

    }

}
