package acnologia.a4nalogia_pokedex.Códigos_Centrais;


import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

import acnologia.a4nalogia_pokedex.R;
import acnologia.a4nalogia_pokedex.Pokemon;

public class Integrador_API extends RecyclerView.Adapter<Integrador_API.ViewHolder> {
private ArrayList<Pokemon> data;
private Context context;
private String habilidades = "abilities";
    public Integrador_API(Context context){
        this.context = context;
        data = new ArrayList<>();


    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lista_pokemon, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Integrador_API.ViewHolder holder, int position) {
        Pokemon poke = data.get(position);

        Pokemon poke2 = data.get(position);

        holder.nomeTextView.setText(poke.getName());
       holder.ability.setText(poke2.getAbilities());
        Glide.with(context)
                .load("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/"+(position + 1)+".png")
                .centerCrop()
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.fotoImagemView);//note

    }

    @Override
    public int getItemCount() {

        return data.size();
    }

    public void adicionar_dados(ArrayList<Pokemon>listaPokemon){
        data.addAll(listaPokemon);
        notifyDataSetChanged();

    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView fotoImagemView;
        private TextView  nomeTextView;
        private TextView  ability;
        private CardView  card;

        public ViewHolder(View itemView){
            super(itemView);

            fotoImagemView =  itemView.findViewById(R.id.photoImageView);
            nomeTextView =  itemView.findViewById(R.id.namaeTextView);
            ability =  itemView.findViewById(R.id.abilityTextView);
            card =  itemView.findViewById(R.id.cards);
        }

    }
}
