package acnologia.a4nalogia_pokedex.Códigos_Centrais;



public class Treinadores
{
    private String mestre;
    private String regiao;
    private String pokemon_start;


    public String getMestre() {
        return mestre;
    }

    public void setMestre(String mestre) {
        this.mestre = mestre;
    }

    public String getRegiao() {
        return regiao;
    }

    public void setRegiao(String regiao) {
        this.regiao = regiao;
    }

    public String getPokemon_start() {
        return pokemon_start;
    }

    public void setPokemon_start(String pokemon_start) {
        this.pokemon_start = pokemon_start;
    }
}
