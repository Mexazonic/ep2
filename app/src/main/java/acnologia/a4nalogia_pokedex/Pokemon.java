package acnologia.a4nalogia_pokedex;

public class Pokemon {

    private int number;
    private String name;
    private String url;
    private String abilities;

    public String getAbilities() {
        return abilities;
    }

    public void setAbilities(String abilities) {
        this.abilities = abilities;
    }

    public int getNumber() {
        return number;
    }

    public int setNumber(int number)
    {
        String[]  urlPartes = url.split("/");
        return Integer.parseInt(urlPartes[urlPartes.length -1]);

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}

